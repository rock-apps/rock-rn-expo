import {Text} from "galio-framework";
import Heading from "../components/UI/Heading";
import React from "react";

/**
 *
 * @param number
 * @param decimals
 * @param prefix
 * @param decimalSeparator Opcional, já está adaptado para padrão BR
 * @param thousantSeparator Opcional, já está adaptado para padrão BR
 * @returns {string}
 */
export const currencyFormat = (number, decimals, prefix, decimalSeparator, thousantSeparator) => {
    decimals = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals,
        decimalSeparator = typeof decimalSeparator === "undefined" ? "," : decimalSeparator;
    thousantSeparator = typeof thousantSeparator === "undefined" ? "." : thousantSeparator;
    let sign = number < 0 ? "-" : "";
    let i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decimals)));
    let j = (j = i.length) > 3 ? j % 3 : 0;

    return sign + (prefix ? prefix + ' ' : '') +
        (j ? i.substr(0, j) + thousantSeparator : "") +
        i.substr(j).replace(/(\decimalSeparator{3})(?=\decimalSeparator)/g, "$1" + thousantSeparator) +
        (decimals ? decimalSeparator + Math.abs(number - i).toFixed(decimals).slice(2) : "");
};

export const addressFormat = (obj) => {
    return `Rua ${obj.address}, Nº ${obj.address_number}, Bairro ${obj.address_district}, ${obj.address_city} - ${obj.address_state}`
}

export const distanceFormat = (distance) => {
    let km = getKm(distance)
    let metro = getMetro(distance)

    return `${km} km e ${metro} m.`

}

export const getKm = (distance) => {
    let km = distance / 1000
    if (km > 0) {
        return parseInt(km)
    }
    return 0
}

export const getMetro = (distance) => {
    let km = distance / 1000
    if (km > 0) {
        return distance % 1000
    }
    return distance
}


export const renderListItem = (label, value) => {
    if (!value) return null;
    return (
        <Text>
            <Heading label={label + ': '} inverted bold/>
            {value}
        </Text>
    )
};

export const qrCodeUri = (data, width, height) => {
    return 'https://api.qrserver.com/v1/create-qr-code/?size=' + (width ? width : 150) + 'x' + (height ? height : 150) + '&data=' + data;
};

export const serialize = function (obj, prefix) {
    let str = [],
        p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            let k = prefix ? prefix + "[" + p + "]" : p,
                v = obj[p];
            str.push((v !== null && typeof v === "object") ?
                serialize(v, k) :
                encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
};