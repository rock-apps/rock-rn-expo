import React from "react";
import {StyleSheet} from "react-native";
import {Block, Text, theme} from "galio-framework";

import Icon from "./Icon";
import argonTheme from "../constants/Theme";

class DrawerItem extends React.Component {
    renderIcon = () => {
        const {title, focused} = this.props;

        const buildIcon = (iconName,iconFamily,size,color) => {

            const colorFinal = color ? color : argonTheme.COLORS.PRIMARY;

            return <Icon name={iconName} family={iconFamily} size={size?size:14} color={focused ? "white" : colorFinal}/>
        };

        switch (title) {
            case "Home":return buildIcon("home","font-awesome");
            case "Perfil":return buildIcon("user","font-awesome");
            case "Financeiro":return buildIcon("usd","font-awesome");
            case "Buscar":return buildIcon("search","font-awesome");
            case "Comprar Pontos":return buildIcon("shopping-cart","font-awesome");
            case "Meus Produtos":return buildIcon("star","font-awesome");
            case "Relatórios":return buildIcon("clipboard","font-awesome");
            case "Minha Empresa":return buildIcon("briefcase","font-awesome");
            case "Transferir Pontos":return buildIcon("exchange","font-awesome");
            case "Validar Voucher":return buildIcon("qrcode","font-awesome");
            case "Sair":return buildIcon("sign-out","font-awesome");
            default:
                return null;
        }
    };

    render() {
        const {focused, title} = this.props;

        const containerStyles = [
            styles.defaultStyle,
            focused ? [styles.activeStyle, styles.shadow] : null
        ];

        return (
            <Block flex row style={containerStyles}>
                <Block middle flex={0.1} style={{marginRight: 5}}>
                    {this.renderIcon()}
                </Block>
                <Block row center flex={0.9}>
                    <Text
                        style={{fontFamily: 'open-sans-regular'}}
                        size={15}
                        bold={focused ? true : false}
                        color={focused ? "white" : "rgba(0,0,0,0.5)"}
                    >
                        {title}
                    </Text>
                </Block>
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    defaultStyle: {
        paddingVertical: 15,
        paddingHorizontal: 14
    },
    activeStyle: {
        backgroundColor: argonTheme.COLORS.ACTIVE,
        borderRadius: 4
    },
    shadow: {
        shadowColor: theme.COLORS.BLACK,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 8,
        shadowOpacity: 0.1
    }
});

export default DrawerItem;
