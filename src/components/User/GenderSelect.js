import React from "react";
import InputSelect from "../UI/InputSelect";
import PropTypes from "prop-types";

class GenderSelect extends React.Component {

    render() {

        const options =[
            { value: "MALE", label: "Masculino" },
            { value: "FEMALE", label: "Feminino" },
            { value: "OTHER", label: "Outros" },
        ];

        return <InputSelect
            value={this.props.value}
            onValueChange={this.props.onValueChange}
            options={options}
            placeholder={"Selecione o Gênero"}
        />
    }
}

GenderSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
    value: PropTypes.any,
};
//export your list as a default export
export default GenderSelect;
