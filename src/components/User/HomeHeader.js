import React from 'react'
import {Block} from "galio-framework";
import PropTypes from "prop-types";
import Avatar from "../UI/Avatar";
import Heading from "../UI/Heading";
import Spinner from "../UI/Spinner";


class HomeHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const user = this.props.user;

        let label = '-';
        if(user) {
            label = 'Olá, ' + user.name;
        }else {
            return null
        }

        return (
            <Block row space="between">
                <Block middle>
                    <Avatar source={user.photo}/>
                </Block>
                <Block middle fluid>
                    <Heading label={label}/>
                </Block>
                {/*<Block middle>*/}
                {/*    <Heading label={user.points_balance ? user.points_balance : '-'} size={'large'} bold/>*/}
                {/*    <Heading label="PONTOS"/>*/}
                {/*</Block>*/}
            </Block>
        );
    }
}

HomeHeader.propTypes = {
    user: PropTypes.object,
};

export default HomeHeader