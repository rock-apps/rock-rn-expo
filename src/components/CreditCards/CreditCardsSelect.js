import React from "react";
import PropTypes from "prop-types";
import Spinner from "../UI/Spinner";
import {getCreditCards} from "../../../actions/creditCard";
import {connect} from "react-redux";
import InputSelect from "../UI/InputSelect";
import AlertNotFound from "../UI/AlertNotFound";

class CreditCardsSelect extends React.Component {

    componentDidMount() {
        this.props.getCreditCards();
    }

    render() {

        const {creditCardLoading, cards, onValueChange, value} = this.props;

        if (creditCardLoading) {
            return (<Spinner/>)
        } else {

            if (cards && cards.length > 0) {
                const cardObj = cards.map((card) => {
                    return {
                        label: card.last_digits + ' ' + card.brand + ' - ' + card.holder_name,
                        value: card.id,
                        key: card.id,
                    }
                });

                return <InputSelect
                    onValueChange={onValueChange}
                    options={cardObj}
                    value={value}
                    placeholder={"Selecione um cartão"}
                />
            } else {
                return <AlertNotFound label='Nenhum cartão de crédito encontrado!'/>
            }
        }
    }

}

CreditCardsSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
};


//Map the redux state to your props.
const mapStateToProps = state => ({
    user: state.User.user,
    token: state.User.token,
    creditCardLoading: state.CreditCard.isLoading,
    cards: state.CreditCard.cards,
});

//Map your action creators to your props.
const mapDispatchToProps = {
    getCreditCards
};

//export your list as a default export
export default connect(mapStateToProps, mapDispatchToProps)(CreditCardsSelect);
