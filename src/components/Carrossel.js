import React from 'react'
import {ActivityIndicator, Dimensions, ScrollView, View} from "react-native";
import {theme} from "galio-framework";
import Spinner from "./UI/Spinner";

const {width, height} = Dimensions.get("screen");
const cardWidth = width - theme.SIZES.BASE * 2;

class Carrossel extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {paging} = this.props
        return (
            <ScrollView
                horizontal={true}
                pagingEnabled={paging ? true : false}
                decelerationRate={0}
                scrollEventThrottle={16}
                snapToAlignment="center"
                showsHorizontalScrollIndicator={false}
                snapToInterval={paging && cardWidth + theme.SIZES.BASE * 0.375 || null}
                contentContainerStyle={
                    {
                        paddingHorizontal: theme.SIZES.BASE / 2
                    }
                }
            >
                {this.props.isLoading ? <Spinner/> : this.props.children}
            </ScrollView>
        )
    }
}

export default Carrossel