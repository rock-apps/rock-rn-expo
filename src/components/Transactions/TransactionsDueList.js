import React from "react";
import PropTypes from "prop-types";
import {Block, Text, theme} from "galio-framework";
import {Dimensions} from "react-native";
import {argonTheme} from "../../constants";
import {DateFormat} from "../UI/DateFormat";
import Spinner from "../UI/Spinner";


const {width, height} = Dimensions.get("screen");
const cardWidth = width - theme.SIZES.BASE * 2;

class TransactionsDueList extends React.Component {
    render() {
        const transactions = this.props.transactions;
        const style = {
            width: cardWidth,
            backgroundColor: 'white',
            marginVertical: 10,
            borderRadius: 4,
            borderLeftWidth: 4,
            borderLeftColor: argonTheme.COLORS.PRIMARY,
            padding: 10,
            alignItems: 'center'
        };

        if (this.props.isLoading) {
            return (<Spinner/>)
        } else {
            return (
                <Block flex>
                    {transactions.length > 0 && transactions.map((transaction) => {
                        return <Block row style={style} key={transaction.id}>
                            <Block center>
                                <Text style={{marginRight: 20}}><DateFormat date={transaction.created_at}
                                                                            format={'DD/MM'}/></Text>
                                <Text style={{marginRight: 20}}><DateFormat date={transaction.created_at}
                                                                            format={'HH:mm'}/></Text>
                            </Block>
                            <Block flex>
                                <Text>{transaction.name} - Nome do Cliente</Text>
                                <Text>Body Tech II Leblon {transaction.created_at}</Text>
                            </Block>
                        </Block>
                        }
                    )}
                </Block>
            );
        }
    }

}

TransactionsDueList.propTypes = {
    transactions: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    isLoading: PropTypes.any,
};


export default TransactionsDueList;
