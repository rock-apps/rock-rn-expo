import React from "react";
import {transactionStatuses} from "../../../actions/transaction";
import PropTypes from "prop-types";

class TransactionStatusLabel extends React.Component {

    render() {

        const {value} = this.props;

        if (!value) return "";
        let item = false;
        transactionStatuses.map((o) => {
            if(o.value === value) item = o;
        });
        if (!item) return value;

        return item.label;
    }
}

TransactionStatusLabel.propTypes = {
    value: PropTypes.string,
};
//export your list as a default export
export default TransactionStatusLabel;
