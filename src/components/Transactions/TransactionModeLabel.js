import React from "react";
import {transactionModes} from "../../../actions/transaction";
import PropTypes from "prop-types";

class TransactionModeLabel extends React.Component {

    render() {

        const {value} = this.props;

        if (!value) return "";
        let item = false;
        transactionModes.map((o) => {
            if(o.value === value) item = o;
        });
        if (!item) return value;

        return item.label;
    }
}

TransactionModeLabel.propTypes = {
    value: PropTypes.string,
};
//export your list as a default export
export default TransactionModeLabel;
