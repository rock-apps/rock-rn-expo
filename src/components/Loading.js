import React from 'react'
import {ActivityIndicator, Dimensions, View} from "react-native";
import Spinner from "./UI/Spinner";
const {width, height} = Dimensions.get("screen");


class Loading extends React.Component {
    render() {
        return (
            <View style={{
                width: width,
                height: height,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'rgba(255,255,255,0.3)',
            }}>
                <Spinner/>
            </View>
        )
    }
}

export default Loading