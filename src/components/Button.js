import React from "react";
import {StyleSheet} from "react-native";
import PropTypes from 'prop-types';
import {Button, Text} from "galio-framework";

import argonTheme from "../constants/Theme";

class ArButton extends React.Component {
  render() {
    const {small, shadowless, children, color, style, fontSize, label, styleLink, ...props} = this.props;

    const colorStyle = color || argonTheme.COLORS.PRIMARY;

    let buttonStyles = [
      small && styles.smallButton,
      color && {backgroundColor: colorStyle},
      !shadowless && styles.shadow,
      shadowless && styles.shadowLess,
      {...style}
    ];


    return (
        <Button
            style={buttonStyles}
            shadowless
            textStyle={{fontSize: fontSize || 12, fontWeight: '700'}}
            {...props}
        >
          {children ? children : <Text
              style={{fontFamily: "open-sans-bold"}}
              size={14}
              color={argonTheme.COLORS.WHITE}
          >{label}</Text>}
        </Button>
    );
  }
}

ArButton.propTypes = {
  small: PropTypes.bool,
  shadowless: PropTypes.bool,
  color: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf(['default', 'primary', 'secondary', 'info', 'error', 'success', 'warning'])
  ]),
  label: PropTypes.string,
  styleLink: PropTypes.bool
}

const styles = StyleSheet.create({
  smallButton: {
    width: 75,
    height: 28
  },
  shadow: {
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
  shadowLess: {
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 0,
  },
});


export default ArButton;
