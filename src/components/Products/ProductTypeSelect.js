import React from "react";
import InputSelect from "../UI/InputSelect";
import {productTypes} from "../../../actions/product";
import PropTypes from "prop-types";
import DistanceSelect from "../Companies/DistanceSelect";

class ProductTypeSelect extends React.Component {

    render() {

        return <InputSelect
            value={this.props.value}
            onValueChange={this.props.onValueChange}
            options={productTypes}
            placeholder={"Tipo de Atividade ou Produto"}
        />
    }
}

ProductTypeSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
    value: PropTypes.any,
};
//export your list as a default export
export default ProductTypeSelect;
