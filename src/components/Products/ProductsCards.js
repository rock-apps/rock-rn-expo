import React from "react";
import PropTypes from "prop-types";
import {Block} from "galio-framework";
import Spinner from "../UI/Spinner";
import AlertNotFound from "../UI/AlertNotFound";
import ProductCardSimple from "./ProductCardSimple";

class ProductsCards extends React.Component {
    render() {
        const {products, isLoading, navigation} = this.props;
        // const style = {paddingHorizontal: theme.SIZES.BASE};
        const style = {};

        return (

            <Block style={style}>
                {isLoading ? <Spinner/> : (
                    products && products.length > 0 ?
                        products.map(product => {
                                return <ProductCardSimple product={product} key={product.id} navigation={navigation} />
                        })
                        : <AlertNotFound label='Nenhum produto encontrado!'/>
                )}
            </Block>
        );
    };

}

ProductsCards.propTypes = {
    products: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    isLoading: PropTypes.any,
};


export default ProductsCards;
