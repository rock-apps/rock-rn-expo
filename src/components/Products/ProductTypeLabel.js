import React from "react";
import {productTypes} from "../../../actions/product";
import PropTypes from "prop-types";

class ProductTypeLabel extends React.Component {

    render() {

        const {value} = this.props;

        if (!value) return "";
        let item = false;
        productTypes.map((productType) => {
            if(productType.value === value) item = productType;
        });
        if (!item) return value;

        return item.label;
    }
}

ProductTypeLabel.propTypes = {
    value: PropTypes.string,
};
//export your list as a default export
export default ProductTypeLabel;
