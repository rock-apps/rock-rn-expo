import React from "react";
import PropTypes from "prop-types";
import {
    StyleSheet,
    Image,
    TouchableWithoutFeedback
} from "react-native";
import {Block, Button, Text, theme} from "galio-framework";
import { Icon } from "../index";
import {argonTheme} from "../../constants";
import Divider from "../UI/Divider";
import NumberCurrency from "../UI/NumberCurrency";

class ProductCard extends React.Component {
    render() {
        const {
            navigation,
            item,
            horizontal,
            full,
            style,
            ctaColor,
            imageStyle,
            ctaRight
        } = this.props;

        const imageStyles = [
            full ? styles.fullImage : styles.horizontalImage,
            imageStyle
        ];
        const cardContainer = [styles.card, styles.shadow, style];
        const imgContainer = [
            styles.imageContainer,
            horizontal ? styles.horizontalStyles : styles.verticalStyles,
            styles.shadow
        ];

        return (
            <Block row={horizontal} card flex style={cardContainer}>
                <TouchableWithoutFeedback
                    onPress={() => navigation.navigate("Company", {company_id: item.company_id})}
                >
                    <Block flex style={imgContainer}>
                        <Image source={{uri: item.image}} style={imageStyles}/>
                    </Block>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                    onPress={() => navigation.navigate("Company", {company_id: item.company_id})}
                >
                    <Block flex space="between" style={styles.cardDescription}>
                        <Block flex>
                            <Text
                                size={14}
                                style={styles.cardTitle}
                                color={argonTheme.COLORS.TEXT}
                                bold
                            >
                                {item.name}
                            </Text>
                            <Text>
                                {/*<Icon*/}
                                {/*    size={14}*/}
                                {/*    family="materialicon"*/}
                                {/*    name="map"*/}
                                {/*    color="black"*/}
                                {/*    style={{marginRight: 5}}*/}
                                {/*/>*/}
                                {item.company.name}
                            </Text>
                            <Divider size={'large'} vmargin={'small'}/>
                            <Text>
                                Preço <NumberCurrency value={item.cost} currency decimals={2}/>
                            </Text>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </Block>
        );
    }
}

ProductCard.propTypes = {
    item: PropTypes.object,
    horizontal: PropTypes.bool,
    full: PropTypes.bool,
    ctaColor: PropTypes.string,
    imageStyle: PropTypes.any,
    ctaRight: PropTypes.bool
};

const styles = StyleSheet.create({
    card: {
        backgroundColor: theme.COLORS.WHITE,
        marginVertical: theme.SIZES.BASE,
        borderWidth: 0,
        minHeight: 114,
        marginBottom: 4,
    },
    cardTitle: {
        // flex: 1,
        // flexWrap: "wrap",
        paddingBottom: 6
    },
    cardDescription: {
        padding: theme.SIZES.BASE / 2
    },
    imageContainer: {
        borderRadius: 3,
        elevation: 1,
        overflow: "hidden"
    },
    image: {
        // borderRadius: 3,
    },
    horizontalImage: {
        height: 122,
        width: "auto"
    },
    horizontalStyles: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    verticalStyles: {
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0
    },
    fullImage: {
        height: 215
    },
    shadow: {
        shadowColor: "#8898AA",
        shadowOffset: {width: 0, height: 1},
        shadowRadius: 6,
        shadowOpacity: 0.1,
        elevation: 2
    }
});

export default ProductCard;
