import React from "react";
import PropTypes from "prop-types";
import {Dimensions,} from "react-native";
import {Block, Button, Text, theme} from "galio-framework";
import {argonTheme} from "../../constants";
import {currencyFormat, renderListItem} from "../../services/Helpers";
import ProductTypeLabel from "./ProductTypeLabel";

class ProductCardSimple extends React.Component {
    render() {
        const {navigation, product} = this.props;

        // const cardContainer = [styles.card, styles.shadow, style];

        const {width, height} = Dimensions.get("screen");
        const cardWidth = width - theme.SIZES.BASE * 2;

        let color = argonTheme.COLORS.PRIMARY;
        if (!product.active) {
            color = argonTheme.COLORS.MUTED;
        }

        const styles = {
            width: cardWidth,
            backgroundColor: 'white',
            marginVertical: 10,
            borderRadius: 4,
            borderLeftWidth: 4,
            borderLeftColor: color,
            paddingHorizontal: 10,
            paddingVertical: 5,
            alignItems: 'center'
        };

        return (

            <Block row style={styles} key={product.id}>
                <Block flex>
                    <Block row>
                        <Block flex>
                            <Text>{product.name} {product.active ? null : ' (Inativo)'}</Text>
                            {/*<Text>{renderListItem('Pontos', currencyFormat(product.points, 0))}</Text>*/}
                            <Text>{renderListItem('Preço', currencyFormat(product.cost, 2, 'R$'))}</Text>
                        </Block>
                        <Block middle style={{paddingHorizontal: 5}}>
                            <Text><ProductTypeLabel value={product.type}/></Text>
                        </Block>
                    </Block>
                </Block>
                <Block middle>
                    <Button onlyIcon icon="pencil" iconFamily="font-awesome" iconSize={30}
                            onPress={() => navigation.navigate('ProductUpdate', {product_id: product.id})}
                            color={color} iconColor="white"
                            style={{width: 40, height: 40}}/>
                </Block>
            </Block>

        );
    }
}

ProductCardSimple.propTypes = {
    product: PropTypes.object,
};

// const styles = StyleSheet.create({
//     card: {
//         backgroundColor: theme.COLORS.WHITE,
//         marginVertical: theme.SIZES.BASE,
//         borderWidth: 0,
//         marginBottom: 4,
//     },
//     cardTitle: {
//         // flex: 1,
//         // flexWrap: "wrap",
//         paddingBottom: 6
//     },
//     cardDescription: {
//         padding: theme.SIZES.BASE / 2
//     },
//     imageContainer: {
//         borderRadius: 3,
//         elevation: 1,
//         overflow: "hidden"
//     },
//     image: {
//         // borderRadius: 3,
//     },
//     horizontalImage: {
//         height: 122,
//         width: "auto"
//     },
//     horizontalStyles: {
//         borderTopRightRadius: 0,
//         borderBottomRightRadius: 0
//     },
//     verticalStyles: {
//         borderBottomRightRadius: 0,
//         borderBottomLeftRadius: 0
//     },
//     fullImage: {
//         height: 215
//     },
//     shadow: {
//         shadowColor: "#8898AA",
//         shadowOffset: {width: 0, height: 1},
//         shadowRadius: 6,
//         shadowOpacity: 0.1,
//         elevation: 2
//     }
// });

export default ProductCardSimple;
