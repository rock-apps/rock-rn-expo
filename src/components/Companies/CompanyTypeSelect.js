import React from "react";
import InputSelect from "../UI/InputSelect";
import {companyTypes} from "../../../actions/company";
import PropTypes from "prop-types";

class CompanyTypeSelect extends React.Component {


    render() {

        return <InputSelect
            value={this.props.value}
            onValueChange={this.props.onValueChange}
            options={companyTypes}
            placeholder={"Tipo de Empresa"}
        />
    }
}

CompanyTypeSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
    value: PropTypes.any,
};

//export your list as a default export
export default CompanyTypeSelect;
