import React from "react";
import PropTypes from "prop-types";
import {Block, theme} from "galio-framework";
import {Carrossel} from "../index";
import Spinner from "../UI/Spinner";
import ProductCard from "./ProductCard";
import {Dimensions} from "react-native";

const {width, height} = Dimensions.get("screen");
const cardWidth = width - theme.SIZES.BASE * 2;

class ProductCarrossel extends React.Component {
    render() {
        const products = this.props.products;

        return (
            <Carrossel isLoading={this.props.isLoading}>
                <Block flex row>
                    {products.length > 0 && products.map(product => <ProductCard
                            key={product.id} item={product}
                            style={{marginRight: theme.SIZES.BASE, width: cardWidth}}
                        />
                    )}
                </Block>
            </Carrossel>
        );
    };

}

ProductCarrossel.propTypes = {
    products: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    isLoading: PropTypes.any,
};


export default ProductCarrossel;
