import React from "react";
import PropTypes from "prop-types";
import {Block, theme} from "galio-framework";
import {TouchableOpacity} from 'react-native'
import {Carrossel} from "../index";
import Spinner from "../UI/Spinner";
import Avatar from "../UI/Avatar";


class CompanyCompaniesAvatarCarrossel extends React.Component {
    render() {

        const {companies, navigation} = this.props;
        return (
            <Carrossel>
                {this.props.isLoading ? <Spinner/> : (
                    <Block flex row>
                        {companies.length > 0 && companies.map(company =>
                            <TouchableOpacity key={company.id}
                                onPress={() => navigation.navigate("Company", {company_id: item.id})}
                            >
                                <Avatar source={company.image_1} key={company.id} style={{marginRight: 20}}/>
                            </TouchableOpacity>
                        )}
                    </Block>
                )}
            </Carrossel>
        );
    };

}

CompanyCompaniesAvatarCarrossel.propTypes = {
    companies: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    isLoading: PropTypes.any,
};


export default CompanyCompaniesAvatarCarrossel;
