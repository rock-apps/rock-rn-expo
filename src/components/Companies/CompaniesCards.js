import React from "react";
import PropTypes from "prop-types";
import {Block} from "galio-framework";
import Spinner from "../UI/Spinner";
import CompanyCard from "./CompanyCard";
import AlertNotFound from "../UI/AlertNotFound";

class CompaniesCards extends React.Component {
    render() {
        const companies = this.props.companies;
        // const style = {paddingHorizontal: theme.SIZES.BASE};
        const style = {};

        return (

            <Block style={style}>

                {this.props.isLoading ? <Spinner/> : (
                    companies.length > 0 ?
                        companies.map(company => <CompanyCard company={company} key={company.id} horizontal/>)
                        : <AlertNotFound label='Nenhuma empresa encontrada!'/>
                )}
            </Block>
        );
    };

}

CompaniesCards.propTypes = {
    companies: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    isLoading: PropTypes.any,
};


export default CompaniesCards;
