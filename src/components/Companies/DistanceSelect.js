import React from "react";
import InputSelect from "../UI/InputSelect";
import PropTypes from "prop-types";
import HeadingWithButton from "../UI/HeadingWithButton";

class DistanceSelect extends React.Component {


    render() {

        const {onValueChange, value} = this.props;

        const options = [
            {
                label: '1 Kilômetro',
                value: 1000,
            },
            {
                label: '3 Kilômetros',
                value: 3000,
            },
            {
                label: '5 Kilômetros',
                value: 5000,
            },
            {
                label: '10 Kilômetros',
                value: 10000,
            },
            {
                label: '20 Kilômetros',
                value: 20000,
            },
            {
                label: 'Sem limite',
                value: 99999999,
            },
        ];

        return <InputSelect
            value={value}
            onValueChange={onValueChange}
            options={options}
            placeholder={"Distância máxima em KM"}
        />
    }
}

DistanceSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
    value: PropTypes.any,
};

//export your list as a default export
export default DistanceSelect;
