import React, {Component} from 'react'
import {Rating} from "react-native-ratings";
import Constants from "../../constants/Theme";

class RatingShow extends Component {

    constructor(props) {
        super(props)
    }

    ratingModalOpen = (order) => {
        this.props.ratingModalOpen()
    };

    isShowModal = () => {
        this.props.isShowModal()
    }

    onRating = () => {
        this.props.onRating()
    }


    render() {
        const color = this.props.color ? this.props.color : Constants.COLORS.SECONDARY;
        const {startingValue, ratingCount, style} = this.props
        let size = 20;
        switch (this.props.size) {
            case 'small':
                size = 10;
                break;
            case 'normal':
                size = 20;
                break;
            case 'large':
                size = 30;
                break;
            case 'xlarge':
                size = 40;
                break;
            case 'xxlarge':
                size = 50;
                break;
        }
        return (
            <Rating
                imageSize={size}
                readonly={true}
                startingValue={startingValue}
                ratingCount={ratingCount || 5}
                style={style}
            />
        )
    }
}

export default RatingShow