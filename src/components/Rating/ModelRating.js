import React, {Component} from 'react'
import {View} from 'react-native'
import {AirbnbRating} from "react-native-ratings";
import InputDefault from "../UI/InputDefault";
import ModalConfirmation from "../UI/ModalConfirmation";

class ModelRating extends Component {

    constructor(props) {
        super(props)
        this.state = {
            transactionModalRatingShow: false,
            selectedTransaction: '',
            transactionModalRatingTitle: 'Avaliação do pedido',
            transactionModalRatingBody: null,
            transactionRating: 0,
            transactionRatingDescription: '',
            orderSelected: 0,
        }
    }

    ratingModalOpen = (order) => {
        this.props.ratingModalOpen()
    };

    isShowModal = () => {
        this.props.isShowModal()
    }

    onRating = () => {
        this.props.onRating()
    }

    render() {
        const {modalRatingTitle, colorConfirm} = this.props
        return (
            <ModalConfirmation
                title={modalRatingTitle}
                body={<View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 10
                    }}>
                        <AirbnbRating
                            reviews={['Terrível!', 'Ruim!', 'Bom', 'Muito bom!', 'Excelente!']}
                            imageSize={30}
                            showRating={true}
                            defaultRating={0}
                            onFinishRating={(value) => this.props.handleRating(value)}
                            style={{marginVertical: 20}}
                        />
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10, flex: 1}}>

                        <InputDefault
                            style={{flex: 1}}
                            onChangeText={(value) => this.props.onChangeDescription(value)}
                            value={this.props.ratingDescription}
                            placeholder="Comentários"
                        />
                    </View>
                </View>}
                isOpen={this.props.isOpen}
                handleCancel={() => {
                    this.props.handleCancel()
                }}
                style={{height: 300}}
                handleConfirm={() => this.onRating()}
                labelConfirm={'Avaliar'}
                colorConfirm={colorConfirm}
            />
        )
    }
}

export default ModelRating