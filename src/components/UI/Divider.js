import React from 'react'
import {Block, theme} from "galio-framework";
import PropTypes from "prop-types";
import Heading from "./Heading";
import {argonTheme} from "../../constants";


class Divider extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let size = '90%';
        switch (this.props.size) {
            case 'small':
                size = '50%';
                break;
            case 'normal':
                size = '90%';
                break;
            case 'large':
                size = '100%';
                break;
        }

        let vmargin = {marginTop: theme.SIZES.BASE, marginBottom: theme.SIZES.BASE};

        switch (this.props.vmargin) {
            case 'tiny':
                vmargin = {marginTop: theme.SIZES.BASE / 2, marginBottom: theme.SIZES.BASE / 2};
                break;
            case 'small':
                vmargin = {marginTop: theme.SIZES.BASE * 2 / 3, marginBottom: theme.SIZES.BASE * 2 / 3};
                break;
            case 'normal':
                vmargin = {marginTop: theme.SIZES.BASE, marginBottom: theme.SIZES.BASE};
                break;
            case 'large':
                vmargin = {marginTop: theme.SIZES.BASE * 3 / 2, marginBottom: theme.SIZES.BASE * 3 / 2};
                break;
            case 'xlarge':
                vmargin = {marginTop: theme.SIZES.BASE * 2, marginBottom: theme.SIZES.BASE * 2};
                break;
        }

        const style = {...this.props.style, ...vmargin};

        let color = "#E9ECEF";
        if (this.props.color) {
            color = this.props.color;
        }
        if (this.props.inverted) {
            color = argonTheme.COLORS.BLACK;
        }

        return (
            <Block middle style={style}>
                {!this.props.noLine ?
                    <Block style={{
                        width: size,
                        borderWidth: 1,
                        borderColor: color
                    }}/>
                    : null}
            </Block>)
    }
}

Divider.propTypes = {
    vmargin: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large'])
    ]),
    inverted: PropTypes.bool,
    color: PropTypes.string,
    noLine: PropTypes.bool,
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large'])
    ]),
};

export default Divider