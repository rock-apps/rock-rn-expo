import React from 'react'
import {Block} from "galio-framework";
import Constants from "../../constants/Theme";
import {Image, StyleSheet, TouchableOpacity} from "react-native";
import {Images} from "../../constants";
import PropTypes from "prop-types";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import ExpoConstants from "expo-constants"


class Avatar extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {editable} = this.props

        if (editable) {
            this.getPermissionAsync()
        }
    }

    getPermissionAsync = async () => {
        if (ExpoConstants.platform.ios) {
            const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            base64: true
        });
        if (!result.cancelled) {
            this.props.onEdit(result)
        }
    };


    render() {

        const color = this.props.color ? this.props.color : Constants.COLORS.SECONDARY;

        let size = 50;
        switch (this.props.size) {
            case 'small':
                size = 30;
                break;
            case 'normal':
                size = 50;
                break;
            case 'large':
                size = 70;
                break;
            case 'xlarge':
                size = 100;
                break;
            case 'xxlarge':
                size = 130;
                break;
        }

        const styles = StyleSheet.create({
            avatarContainer: {
                position: "relative",
            },
            avatar: {
                width: size,
                height: size,
                borderRadius: size / 2,
                borderWidth: 1,
                borderColor: color,
                backgroundColor: 'rgba(0,0,0,0.2)',
            },
        });

        const source = this.props.source ? {uri: this.props.source} : Images.IconBranco;

        const style = {...this.props.style, ...styles.avatarContainer};
        const {editable} = this.props;

        if (editable) {
            return (
                <TouchableOpacity style={style}
                    onPress={() => this._pickImage()}
                >
                    <Image
                        source={source}
                        style={styles.avatar}
                    />
                </TouchableOpacity>
            )
        } else {

            return (

                <Block middle style={style}>
                    <Image
                        source={source}
                        style={styles.avatar}
                    />
                </Block>
            );
        }
    }
}

Avatar.propTypes = {
    source: PropTypes.string,
    editable: PropTypes.bool,
    onEdit: PropTypes.func,
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large', 'xlarge'])
    ]),
};

export default Avatar