import React from 'react'
import PropTypes from "prop-types";
import InputMasked from "./InputMasked";

class InputDocument extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {value, placeholder} = this.props;

        let mask = null
        if (value.length && value.length <= 14) {
            mask = '999.999.999-990';
        } else {
            mask = '99.999.999/9999-99';
        }


        return (
            <InputMasked

                type={'custom'}
                keyboardType={'numeric'}
                placeholder={placeholder ? placeholder : 'CPF ou CNPJ'}
                options={{
                    mask: mask
                }}
                {...this.props}
            />
        );
    }
}

InputDocument.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    flex: PropTypes.any,
    row: PropTypes.any,
    placeholder: PropTypes.string,
    iconName: PropTypes.string,
    iconFamily: PropTypes.string,
    value: PropTypes.any,
    type: PropTypes.string,
    options: PropTypes.object,
};

export default InputDocument