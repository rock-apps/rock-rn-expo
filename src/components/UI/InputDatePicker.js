import React from "react";
import PropTypes from "prop-types";
import DateTimePicker from "react-native-modal-datetime-picker";
import InputDefault from "./InputDefault";
import moment from 'moment';
import 'moment/locale/pt-br';
import { Appearance, useColorScheme } from 'react-native-appearance';

class InputDatePicker extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false
        };
    }

    showDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: true});
    };

    hideDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: false});
    };

    handleDatePicked = date => {
        // console.log("A date has been picked: ", date);

        const dateObj = moment(date);

        if (!dateObj.isValid()) {
            return null;
        }

        if (this.props.mode === 'date') {
            this.props.onChangeText(dateObj.format('YYYY-MM-DD'));
        }
        if (this.props.mode === 'time') {
            this.props.onChangeText(dateObj.format('HH:mm') + ':00');
        }
        if (this.props.mode === 'datetime') {
            this.props.onChangeText(dateObj.format('YYYY-MM-DD HH:mm') + ':00');
        }

        this.hideDateTimePicker();
    };

    renderValue = date => {
        // console.log("A date has been updated: ", date);

        const {mode} = this.props;


        if (mode === 'time') {
            const dateItems = date.split(':');
            return dateItems[0] + ':' + dateItems[1];
        }

        // Não pode testar a validade no timepicker pois dá erro - ele só traz o time e não a data
        const dateObj = moment(date);
        if (!dateObj.isValid()) {
            return null;
        }

        let format = 'DD/MM/YYYY';
        if (mode === 'date') {
            format = 'DD/MM/YYYY';
        }
        if (mode === 'datetime') {
            format = 'DD/MM/YYYY HH:mm';
        }

        return dateObj.format(format);
    };

    render() {

        const {value, placeholder, mode, title, iconFamily, iconName} = this.props;

        if (!mode) {
            throw Error('Modo Inválido');
        }

        return (
            <>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    titleIOS={title ? title : 'Selecione uma Data'}
                    isDarkModeEnabled={Appearance.getColorScheme() === 'dark'}
                    mode={mode}
                    style={{color: 'red'}}
                />
                <InputDefault onChangeText={()=>{}}
                              value={this.renderValue(value)}
                              placeholder={placeholder}
                              iconFamily={iconFamily}
                              iconName={iconName}
                              onTouchStart={this.showDateTimePicker}
                />
            </>
        );
    }

}

InputDatePicker.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    iconName: PropTypes.string,
    iconFamily: PropTypes.string,
    value: PropTypes.any,
    title: PropTypes.string,
    mode: PropTypes.oneOf(['date', 'time', 'datetime']).isRequired
};
export default InputDatePicker