import React from 'react'
import {Dimensions} from "react-native";
import {Block, theme} from "galio-framework";
import PropTypes from "prop-types";
import {Button, Icon, Input} from "../index";
import {argonTheme} from "../../constants";

class FormButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {width} = Dimensions.get("screen");

        const {label, onPress, color, disabled, fullWidth} = this.props;


        let vmargin = {marginTop: 16, marginBottom: 16};
        switch (this.props.vmargin) {
            case 'tiny':
                vmargin = {marginTop: 6, marginBottom: 6};
                break;
            case 'small':
                vmargin = {marginTop: 12, marginBottom: 12};
                break;
            case 'normal':
                vmargin = {marginTop: 16, marginBottom: 16};
                break;
            case 'large':
                vmargin = {marginTop: 30, marginBottom: 30};
                break;
        }


        const style = {
            marginTop: 25,
            marginBottom: 5,
            width: width * 0.5,
            ...this.props.style,
            ...vmargin,
        };

        if (fullWidth) {
            style.width = width - theme.SIZES.BASE * 2;
        }


        let btnColor = color ? color : 'primary';
        if (disabled) {
            btnColor = 'transparent';
            style.backgroundColor = argonTheme.COLORS.MUTED;
        }

        return (
            <Block flex style={{marginBottom: 15}}>
                <Button color={btnColor} style={style}
                        onPress={onPress}
                        label={label}
                        disabled={disabled}
                />
            </Block>
        )
    }
}

FormButton.propTypes = {
    onPress: PropTypes.func.isRequired,
    label: PropTypes.string,
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    margin: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large'])
    ]),
    // iconName: PropTypes.string,
    // iconFamily: PropTypes.string,
};
export default FormButton