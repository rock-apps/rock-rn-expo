import React from 'react'
import {Dimensions, Platform, StyleSheet} from "react-native";
import {Block} from "galio-framework";
import PropTypes from "prop-types";
import {argonTheme} from "../../constants";
import RNPickerSelect from 'react-native-picker-select';

class InputSelect extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {fullWidth} = Dimensions.get("screen");

        const {value, placeholder, onValueChange, width, options} = this.props;


        const style = {
            borderRadius: 4,
            borderColor: argonTheme.COLORS.BORDER,
            // height: 44,
            backgroundColor: '#FFFFFF',
            height: 50,
            // width: 300,
            paddingHorizontal: 14,
            paddingTop: 0,
            paddingBottom: 5,
            shadowColor: "rgba(0, 0, 0, 0.1)",
            shadowOffset: {width: 0, height: 2},
            shadowRadius: 4,
            shadowOpacity: 1,
            marginTop: 5,
            marginBottom: 10, ...this.props.style,
            elevation: 2,
        };

        if(Platform.OS === 'ios') {
            style.paddingTop=10;
        }

        const placeholderObj = {
            label: placeholder ? placeholder : "Selecione uma opção",
            value: null,
            color: argonTheme.COLORS.HEADER
        };

        const pickerStyle = StyleSheet.create({
            inputIOS: {
                fontSize: 16,
                paddingVertical: 12,
                paddingHorizontal: 0,
                borderWidth: 0,
                borderColor: 'gray',
                borderRadius: 0,
                color: 'black',
                paddingRight: 0, // to ensure the text is never behind the icon
            },
            inputAndroid: {
                fontSize: 16,
                paddingHorizontal: 0,
                paddingVertical: 2,
                borderWidth: 0,
                borderColor: 'gray',
                borderRadius: 0,
                color: 'black',
                paddingRight: 0, // to ensure the text is never behind the icon
            },
        });

        return (
            <Block
                width={width ? width : fullWidth}
                style={style}
                flex={this.props.flex}
                row={this.props.row}>

                <RNPickerSelect
                    style={pickerStyle}
                    value={value}
                    placeholder={placeholderObj}
                    onValueChange={onValueChange}
                    items={options}
                />

            </Block>
        )
    }
}

InputSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    options: PropTypes.array,
    style: PropTypes.object,
    value: PropTypes.any,
};
export default InputSelect