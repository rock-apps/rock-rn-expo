import React from 'react'
import {Text} from "galio-framework";
import Constants from "../../constants/Theme";
import PropTypes from "prop-types";

class Heading extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let size = 14;
        switch (this.props.size) {
            case 'small':
                size = 12;
                break;
            case 'normal':
                size = 16;
                break;
            case 'large':
                size = 24;
                break;
            case 'xlarge':
                size = 30;
                break;
            case 'xxlarge':
                size = 38;
                break;
        }

        let style = {...this.props.style};
        if (this.props.vmargin) {
            style = {...style, marginTop: 30, marginBottom: 15};
        }

        let color = this.props.color ? this.props.color : Constants.COLORS.SECONDARY;
        if(this.props.inverted) {
           color =  Constants.COLORS.BLACK;
        }

        return (
            <Text {...this.props} style={style} size={size} color={color} bold={this.props.bold}>
                {this.props.label + '' && this.props.label + '' || this.props.children}
            </Text>
        );
    }
}

Heading.propTypes = {
    inverted: PropTypes.bool,
    vmargin: PropTypes.bool,
    bold: PropTypes.bool,
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large', 'xlarge'])
    ]),
};

export default Heading