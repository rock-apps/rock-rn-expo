import React from 'react'
import Modal from 'react-native-modalbox';
import {Text} from "galio-framework";
import Spinner from "./Spinner";
import {argonTheme} from "../../constants";

class ModalSpinner extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {isOpen, label} = this.props;

        const style = {
            backgroundColor: argonTheme.COLORS.MODAL_BACKGROUND,
            justifyContent: 'center',
            alignItems: 'center',
            height: 200,
            width: 300,
            borderRadius: 4,
            paddingTop: 10,
        };

        return (
            <Modal style={style} position={"center"} isOpen={isOpen}
                   coverScreen={true} backdropPressToClose={false}>
                <Text style={{color: "black", fontSize: 22}}>{label ? label : 'Carregando'}</Text>
                <Spinner color='#000'/>
            </Modal>
        )
    }
}

export default ModalSpinner