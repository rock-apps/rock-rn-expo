import React from 'react'
import PropTypes from "prop-types";
import {currencyFormat} from "../../services/Helpers";


class NumberCurrency extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const prefix = this.props.currency ? 'R$' : '';
        const decimals = this.props.decimals ? this.props.decimals : 0;

        // {/*<NumberFormat value={2456981} displayType={'text'} thousandSeparator={true} prefix={prefix} />*/}


        return (
            currencyFormat(this.props.value, prefix,decimals)
        );
    }
}

NumberCurrency.propTypes = {
    value: PropTypes.number,
    currency: PropTypes.bool,
};

export default NumberCurrency