import React from 'react'
import PropTypes from "prop-types";
import {Button} from "../index";
import {argonTheme} from "../../constants";
import {Block} from "galio-framework";
import {Dimensions} from "react-native";

class Panel extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {label, onPress, color} = this.props;

        const {width, height} = Dimensions.get("screen");

        const style = {
            paddingTop:10,
            width: width * 0.9,
            backgroundColor: "#F4F5F7",
            borderRadius: 4,
            shadowColor: argonTheme.COLORS.BLACK,
            shadowOffset: {
                width: 0,
                height: 4
            },
            shadowRadius: 8,
            shadowOpacity: 0.1,
            elevation: 1,
            // overflow: "hidden"
        };

        return (
            <Block center style={style}>
                {this.props.children}
            </Block>
        )
    }
}

Panel.propTypes = {
    color: PropTypes.string,
};
export default Panel