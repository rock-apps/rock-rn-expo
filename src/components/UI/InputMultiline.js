import React from 'react'
import {Block} from "galio-framework";
import PropTypes from "prop-types";
import {Icon, Input} from "../index";

class InputMultiline extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {

        const {value, placeholder, line, onChangeText, width, password, onTouchStart, keyboardType} = this.props;

        const style = {marginBottom: 5, ...this.props.style};

        let iconContent = false;

        return (
            <Block
                width={width ? width : null}
                style={style}
                flex={this.props.flex}
                row={this.props.row}>
                <Input borderless value={value}
                       multiline
                       textAlignVertical={"top"}
                       numberOfLines={line || 4}
                       placeholder={placeholder}
                       onChangeText={onChangeText}
                       iconContent={iconContent}
                       password={password}
                       onTouchStart={onTouchStart}
                       keyboardType={keyboardType}
                       style={{height: 150, padding: 0}}
                />
            </Block>
        )
    }
}

InputMultiline.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    flex: PropTypes.any,
    row: PropTypes.any,
    style: PropTypes.object,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    password: PropTypes.bool,
    onTouchStart: PropTypes.func,
    keyboardType: PropTypes.string,
};
export default InputMultiline