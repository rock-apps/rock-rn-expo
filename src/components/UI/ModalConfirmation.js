import React from 'react'
import Modal from 'react-native-modalbox';
import {Block, Text} from "galio-framework";
import PropTypes from "prop-types";
import FormButton from "./FormButton";
import Heading from "./Heading";
import Divider from "./Divider";
import COLORS from "galio-framework/src/theme/colors";
import {Button} from "../index";
import {argonTheme} from "../../constants";

class ModalConfirmation extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {isOpen, title, body, handleCancel, handleConfirm, style, labelConfirm, labelCancel, modalBackground, colorConfirm, colorCancel} = this.props;

        if (!title && !body) {
            return null;
        }

        const styleObj = {
            backgroundColor: modalBackground || argonTheme.COLORS.MODAL_BACKGROUND,
            justifyContent: 'center',
            alignItems: 'center',
            height: 200,
            width: 300,
            borderRadius: 4,
            paddingTop: 10,
            ...style
        };

        const styleButtonConfirm = {
            width: 100
        };
        const styleButtonCancel = {
            width: 100,
            backgroundColor: colorCancel || COLORS.GREY
        };

        return (
            <Modal style={styleObj} position={"center"} isOpen={isOpen} coverScreen={true} backdropPressToClose={false}
                   backButtonClose={false}>
                <Heading size={'large'} label={title} inverted/>
                <Divider vmargin={'tiny'}/>
                <Block flex center style={{padding: 5, width: argonTheme.MODAL_WIDTH}}>{body}</Block>
                <Block row style={{margin: 5}}>
                    <Block flex>
                        <Button
                            onPress={handleCancel}
                            label={labelCancel ? labelCancel : 'Cancelar'}
                            color={'transparent'}
                            style={styleButtonCancel}
                        />
                    </Block>
                    <Block flex right>
                        <Button style={styleButtonConfirm}
                                onPress={handleConfirm}
                                label={labelConfirm ? labelConfirm : 'Confirmar'}
                                color={colorConfirm || 'primary'}
                        />
                    </Block>

                </Block>
            </Modal>
        )
    }
}

ModalConfirmation.propTypes = {
    isLoading: PropTypes.any,
    isOpen: PropTypes.bool,
    title: PropTypes.string,
    body: PropTypes.any,
    handleCancel: PropTypes.func.isRequired,
    handleConfirm: PropTypes.func.isRequired,
    labelCancel: PropTypes.string,
    labelConfirm: PropTypes.string,
};

export default ModalConfirmation