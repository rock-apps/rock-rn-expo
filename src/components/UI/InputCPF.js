import React from 'react'
import PropTypes from "prop-types";
import InputMasked from "./InputMasked";

class InputCPF extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <InputMasked
                type={'cpf'}
                {...this.props}
            />
        )
    }
}

InputCPF.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    flex: PropTypes.any,
    row: PropTypes.any,
    placeholder: PropTypes.string,
    iconName: PropTypes.string,
    iconFamily: PropTypes.string,
    value: PropTypes.any,
    type: PropTypes.string,
    options: PropTypes.object,
};
export default InputCPF