import React from 'react'
import {Block, Text} from "galio-framework";
import Constants from "../../constants/Theme";
import PropTypes from "prop-types";
import {ScrollView, TouchableOpacity} from "react-native";

class HeadingWithButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let size = 14;
        switch (this.props.size) {
            case 'small':
                size = 12;
                break;
            case 'normal':
                size = 16;
                break;
            case 'large':
                size = 24;
                break;
            case 'xlarge':
                size = 30;
                break;
            case 'xxlarge':
                size = 38;
                break;
        }

        let style = {alignItems: 'space-between', ...this.props.style};
        if (this.props.vmargin) {
            style = {...style, marginTop: 30, marginBottom: 15};
        }

        const color = this.props.color ? this.props.color : Constants.COLORS.SECONDARY;

        return (
            <Block flex row style={style}>
                <Text {...this.props} style={{flex: 1}} size={size} color={color}>
                    {this.props.label}
                </Text>
                <TouchableOpacity
                    onPress={() => this.props.onPressLabel()}
                >
                    <Text color={color}>{this.props.labelAction}</Text>
                </TouchableOpacity>
            </Block>
        );
    }
}

HeadingWithButton.propTypes = {
    vmargin: PropTypes.bool,
    labelAction: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    onPressLabel: PropTypes.func,
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large', 'xlarge'])
    ]),
};

export default HeadingWithButton