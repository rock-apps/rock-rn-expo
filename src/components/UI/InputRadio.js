import React from 'react'
import {Dimensions, StyleSheet} from "react-native";
import {Block, Radio, Text} from "galio-framework";
import PropTypes from "prop-types";
import {argonTheme} from "../../constants";
import RNPickerSelect from 'react-native-picker-select';
import RadioForm from "react-native-simple-radio-button";

class InputRadio extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {value, placeholder, onChange, options} = this.props;

        return (
            <Block>
                <Text>{placeholder}</Text>
                <Block style={{marginTop: 5}}>
                    <RadioForm
                        radio_props={options}
                        // initial={0}
                        formHorizontal={false}
                        labelHorizontal={true}
                        buttonSize={16}
                        buttonColor={argonTheme.COLORS.MUTED}
                        selectedButtonColor={argonTheme.COLORS.PRIMARY}
                        animation={true}
                        onPress={onChange}
                    />
                </Block>
            </Block>
        )
    }
}

InputRadio.propTypes = {
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    options: PropTypes.array,
    style: PropTypes.object,
    value: PropTypes.any,
};
export default InputRadio