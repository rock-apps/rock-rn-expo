import React from "react";
import {StyleSheet, TouchableOpacity} from "react-native";
import PropTypes from 'prop-types';
import {Block, Checkbox as GCheckbox, Text} from "galio-framework";
import argonTheme from "../../constants/Theme";

class Checkbox extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        const {checkboxStyle, color, colorLabel, label, onChange } = this.props
        return (
            <GCheckbox checkboxStyle={checkboxStyle}
                      color={color || argonTheme.COLORS.PRIMARY}
                      labelStyle={{
                          color: colorLabel || argonTheme.COLORS.TEXT,
                          fontFamily: "open-sans-regular"
                      }}
                      label={label}
                      onChange={onChange}
            />
        );
    }
}


export default Checkbox;
