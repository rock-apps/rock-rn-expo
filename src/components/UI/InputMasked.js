import React from 'react'
import {Block, theme} from "galio-framework";
import PropTypes from "prop-types";
import {Icon, Input} from "../index";
import {TextInputMask} from 'react-native-masked-text'
import {StyleSheet} from "react-native";
import {argonTheme} from "../../constants";

class InputMasked extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {value, placeholder, iconName, iconFamily, onChangeText, width, type, options, shadowless, success, error, keyboardType} = this.props;

        let iconContent = false;
        if (iconFamily && iconName) {
            iconContent = <Icon size={16} color="#ADB5BD" name={iconName} family={iconFamily}
                                style={{marginRight: 12}}/>
        }

        const styles = StyleSheet.create({
            input: {
                borderRadius: 4,
                paddingHorizontal: theme.SIZES.BASE,
                borderColor: argonTheme.COLORS.BORDER,
                height: 44,
                backgroundColor: '#FFFFFF',
                color: argonTheme.COLORS.HEADER,
                marginBottom: 10, ...this.props.style,
            },
            success: {
                borderColor: argonTheme.COLORS.INPUT_SUCCESS,
            },
            error: {
                borderColor: argonTheme.COLORS.INPUT_ERROR,
            },
            shadow: {
                shadowColor: argonTheme.COLORS.BLACK,
                shadowOffset: {width: 0, height: 0.5},
                shadowRadius: 1,
                shadowOpacity: 0.13,
                elevation: 2,
            }
        });


        const inputStyles = [
            styles.input,
            !shadowless && styles.shadow,
            success && styles.success,
            error && styles.error,
            {...this.props.style},
        ];

        return (
            <Block
                width={width ? width : null}
                style={{
                    marginBottom: theme.SIZES.BASE / 2,
                    marginTop: theme.SIZES.BASE / 2,
                }}
                flex={this.props.flex}
                row={this.props.row}>

                <TextInputMask
                    type={type}
                    options={options}
                    keyboardType={keyboardType}
                    placeholderTextColor={argonTheme.COLORS.MUTED}
                    // color={argonTheme.COLORS.HEADER}
                    style={inputStyles}
                    borderless
                    value={value}
                    placeholder={placeholder}
                    onChangeText={onChangeText}
                    iconContent={iconContent}
                />
            </Block>
        )
    }
}

InputMasked.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    flex: PropTypes.any,
    row: PropTypes.any,
    placeholder: PropTypes.string,
    iconName: PropTypes.string,
    iconFamily: PropTypes.string,
    value: PropTypes.any,
    type: PropTypes.string,
    options: PropTypes.object,
};
export default InputMasked