import React from 'react'
import PropTypes from "prop-types";
import {Button} from "../index";
import {argonTheme} from "../../constants";

class FormButtonLink extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {label, onPress, color} = this.props;

        return (
            <Button
                color="transparent"
                shadowless
                textStyle={{
                    color: color ? color : argonTheme.COLORS.PRIMARY,
                    fontSize: 14,
                    fontFamily: "open-sans-regular",
                    marginRight: 5
                }}
                onPress={onPress}
            >
                {label}
            </Button>
        )
    }
}

FormButtonLink.propTypes = {
    onPress: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
    color: PropTypes.string,
};
export default FormButtonLink