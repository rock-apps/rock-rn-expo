import React from 'react'
import Modal from 'react-native-modalbox';
import {Block, Text} from "galio-framework";
import PropTypes from "prop-types";
import FormButton from "./FormButton";
import Heading from "./Heading";
import Divider from "./Divider";
import COLORS from "galio-framework/src/theme/colors";
import {Button} from "../index";
import {argonTheme} from "../../constants";

class ModalSingle extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {isOpen, title, body, handleClose, style} = this.props;

        if (!title) {
            // console.log('Modal Incompleto');
            return null;
        }

        const styleObj = {
            backgroundColor: argonTheme.COLORS.MODAL_BACKGROUND,
            justifyContent: 'center',
            alignItems: 'center',
            height: 200,
            width: 300,
            borderRadius: 4,
            paddingTop: 10,
            ...style
        };

        const styleButtonClose = {
            width: 100,
            backgroundColor: COLORS.GREY
        };

        return (
            <Modal style={styleObj} position={"center"} isOpen={isOpen} coverScreen={true} backdropPressToClose={false}
                   backButtonClose={false}>
                <Heading size={'large'} label={title} inverted/>
                {body ? <Divider vmargin={'tiny'}/> : null}
                {body ? <Block flex center style={{paddingHorizontal:5}}>{body}</Block> : null}
                <Block row center middle style={{margin: 5}}>
                    <Button
                        onPress={handleClose}
                        label={'Fechar'}
                        color={'transparent'}
                        style={styleButtonClose}
                    />

                </Block>
            </Modal>
        )
    }
}

ModalSingle.propTypes = {
    isLoading: PropTypes.any,
    isOpen: PropTypes.bool,
    title: PropTypes.string,
    body: PropTypes.any,
    handleClose: PropTypes.func.isRequired,
};

export default ModalSingle