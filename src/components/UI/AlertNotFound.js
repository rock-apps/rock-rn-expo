import React, {Component} from 'react'
import {Dimensions} from "react-native";
import {Block, Text, theme} from "galio-framework";
import {argonTheme} from '../../constants';
import {Icon} from "../index";
import PropTypes from "prop-types";

class AlertNotFound extends Component {
    _isMounted = false
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    render() {

        const {width, height} = Dimensions.get("screen");

        const label = this.props.label ? this.props.label : 'Não foram encontrados resultados';
        return (

            <Block center style={{
                width: width * 0.9,
                // width: width,
                backgroundColor: "#F4F5F7",
                borderRadius: 4,
                shadowColor: argonTheme.COLORS.BLACK,
                shadowOffset: {
                    width: 0,
                    height: 4
                },
                shadowRadius: 8,
                shadowOpacity: 0.1,
                elevation: 1,
                overflow: "hidden"
            }}>

                <Block middle>
                    <Text muted size={16}
                          style={{paddingTop: theme.SIZES.BASE / 2, paddingBottom: theme.SIZES.BASE / 4}}
                          color={argonTheme.COLORS.TEXT}>
                        {label}
                    </Text>
                    {this._isMounted &&
                        <Icon
                        size={24}
                        style={{marginBottom: theme.SIZES.BASE / 2, marginTop: theme.SIZES.BASE / 2}}
                        color={argonTheme.COLORS.ICON}
                        name="search"
                        family="font-awesome"
                    />}
                </Block>
            </Block>
        )
    }
}

AlertNotFound.propTypes = {
    label: PropTypes.string,
};

export default AlertNotFound