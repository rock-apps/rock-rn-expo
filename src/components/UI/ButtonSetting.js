import React from "react";
import {StyleSheet, TouchableOpacity} from "react-native";
import PropTypes from 'prop-types';
import {Block, Button, Text} from "galio-framework";

import argonTheme from "../../constants/Theme";

class ButtonSetting extends React.Component {
    render() {
        const {small, shadowless, children, color, style, fontSize, label, styleLink, ...props} = this.props;

        const colorStyle = color && argonTheme.COLORS[color.toUpperCase()];

        let buttonStyles = [
            small && styles.smallButton,
            color && {backgroundColor: colorStyle},
            !shadowless && styles.shadow,
            shadowless && styles.shadowLess,
            {...style}
        ];


        return (
            <TouchableOpacity
                onPress={() => this.props.action()}
            >
            <Block flex row space={"around"} style={{paddingVertical: 15, paddingHorizontal: 20, backgroundColor: 'white', borderBottomWidth: 1, borderBottomColor: 'grey'}}>
                <Block flex left>
                    <Text>{this.props.label}</Text>
                </Block>
                <Block flex right>
                    <Text>></Text>
                </Block>
            </Block>
            </TouchableOpacity>
        );
    }
}

ButtonSetting.propTypes = {
    small: PropTypes.bool,
    shadowless: PropTypes.bool,
    color: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['default', 'primary', 'secondary', 'info', 'error', 'success', 'warning'])
    ]),
    label: PropTypes.string,
    styleLink: PropTypes.bool
}

const styles = StyleSheet.create({
    smallButton: {
        width: 75,
        height: 28
    },
    shadow: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 4,
        shadowOpacity: 0.1,
        elevation: 2,
    },
    shadowLess: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 4,
        shadowOpacity: 0.1,
        elevation: 0,
    },
});


export default ButtonSetting;
