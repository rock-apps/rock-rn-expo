import React from 'react'
import PropTypes from "prop-types";
import {Input} from "galio-framework";

class InputSearch extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {value, placeholder} = this.props;


        return (
            <Input
                keyboardType={'web-search'}
                placeholder={placeholder ? placeholder : 'Buscar'}
                {...this.props}
            />
        );
    }
}

InputSearch.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    flex: PropTypes.any,
    row: PropTypes.any,
    placeholder: PropTypes.string,
    iconName: PropTypes.string,
    iconFamily: PropTypes.string,
    value: PropTypes.any,
    type: PropTypes.string,
    options: PropTypes.object,
};

export default InputSearch