import React from 'react'
import {Block, Text} from "galio-framework";
import Constants from "../../constants/Theme";
import PropTypes from "prop-types";
import {distanceFormat} from "../../services/Helpers";
import {StyleSheet} from "react-native";
import {argonTheme} from "../../constants";

class LabelInfo extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let size = 14;
        switch (this.props.size) {
            case 'small':
                size = 12;
                break;
            case 'normal':
                size = 16;
                break;
            case 'large':
                size = 24;
                break;
            case 'xlarge':
                size = 30;
                break;
            case 'xxlarge':
                size = 38;
                break;
        }

        let style = {...this.props.style};
        if (this.props.vmargin) {
            style = {...style, marginTop: 30, marginBottom: 15};
        }

        let color = this.props.color ? this.props.color : Constants.COLORS.SECONDARY;
        if(this.props.inverted) {
            color =  Constants.COLORS.BLACK;
        }

        return (
            <Block flex>
                <Text style={styles.cardLabel}>{this.props.label}</Text>
                <Text style={{
                    fontSize: 10,
                    fontWeight: 'bold',
                    marginBottom: 5
                }}>{this.props.info}</Text>
            </Block>
        );
    }
}

LabelInfo.propTypes = {
    inverted: PropTypes.bool,
    vmargin: PropTypes.bool,
    bold: PropTypes.bool,
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf(['small', 'normal', 'large', 'xlarge'])
    ]),
};

const styles = StyleSheet.create({
    cardLabel: {
        fontSize: 14,
        marginBottom: 5,
        color: argonTheme.COLORS.MUTED,
        fontWeight: 'bold'
    },
});

export default LabelInfo