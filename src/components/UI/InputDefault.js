import React from 'react'
import {Block} from "galio-framework";
import PropTypes from "prop-types";
import {Icon, Input} from "../index";

class InputDefault extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {

        const {value, placeholder, iconName, iconFamily, onChangeText, width, password, onTouchStart, keyboardType, iconColor, placeholderColor, color} = this.props;

        const style = {marginBottom: 5, ...this.props.style};

        let iconContent = false;
        if (iconFamily && iconName) {
            iconContent = this._isMounted && <Icon size={16} color="#ADB5BD" name={iconName} family={iconFamily}
                                style={{marginRight: 12}}/>
        }

        return (
            <Block
                width={width ? width : null}
                style={style}
                flex={this.props.flex}
                row={this.props.row}>
                <Input borderless
                       value={value}
                       placeholder={placeholder}
                       onChangeText={onChangeText}
                       iconContent={iconContent}
                       password={password}
                       onTouchStart={onTouchStart}
                       keyboardType={keyboardType}
                       iconColor={iconColor}
                       placeholderColor={placeholderColor}
                       color={color}
                />
            </Block>
        )
    }
}

InputDefault.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    flex: PropTypes.any,
    row: PropTypes.any,
    style: PropTypes.object,
    placeholder: PropTypes.string,
    iconName: PropTypes.string,
    iconFamily: PropTypes.string,
    value: PropTypes.any,
    password: PropTypes.bool,
    onTouchStart: PropTypes.func,
    keyboardType: PropTypes.string,
};
export default InputDefault