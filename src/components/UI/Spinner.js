import React from 'react'
import {ActivityIndicator} from "react-native";
import {Block} from "galio-framework";
import {argonTheme} from '../../constants';


class Spinner extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let color = argonTheme.COLORS.SECONDARY;
        if (this.props.color) {
            color = this.props.color;
        }
        return (<Block flex middle><ActivityIndicator size="large" color={color}/></Block>)
    }
}

export default Spinner