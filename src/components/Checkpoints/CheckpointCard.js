import React from "react";
import {withNavigation} from "react-navigation";
import PropTypes from "prop-types";
import {
    StyleSheet,
    Image,
    TouchableWithoutFeedback, TouchableOpacity
} from "react-native";
import {Block, Text, theme} from "galio-framework";

import {argonTheme, Images} from "../../constants";
import {Button} from "../index";
import Heading from "../UI/Heading";
import LabelInfo from "../UI/LabelInfo";
import Divider from "../UI/Divider";

class CheckpointCard extends React.Component {

    constructor(props){
        super(props);

    }

    render() {
        const {navigation, index, checkpoint} = this.props;

        return (
            <Block card key={checkpoint.id} style={[styles.shadow, styles.card, {
                padding: theme.SIZES.BASE,
                borderLeftColor: 'blue',
                borderLeftWidth: 8
            }]}>
                <Block flex row space={'between'}>
                    <Block flex={1} center
                           style={{backgroundColor: 'blue', borderRadius: 4, padding: 3}}>
                        <Text style={{fontSize: 8}}>{checkpoint.status}</Text>
                    </Block>
                    <Block flex={5}>
                        <Heading
                            inverted
                            size={'large'}
                            label={`Checkpoint #${index + 1}`}
                            style={{width: '100%', textAlign: 'center'}}
                        />
                    </Block>
                </Block>
                <Block flex style={{
                    width: '100%',
                }}>
                    <Text style={styles.cardLabel}>Nome de contato</Text>
                    <Text style={{fontWeight: 'bold'}}>{checkpoint.contact_name || null}</Text>
                </Block>
                <Block flex row space={'between'} style={{marginVertical: 5}}>
                    <LabelInfo
                        label={`Telefone`}
                        info={checkpoint.contact_phone}
                    />
                    <LabelInfo
                        label={`Celular`}
                        info={checkpoint.contact_mobile}
                    />
                </Block>
                <Divider vmargin={"tiny"}/>
                <Block flex row space={'between'} style={{marginVertical: 5}}>
                    <LabelInfo
                        label={`Rua`}
                        info={checkpoint.address}
                    />
                    <LabelInfo
                        label={`Nº`}
                        info={checkpoint.address_number}
                    />
                </Block>
                <Block flex row space={'between'} style={{marginTop: 5}}>
                    <LabelInfo
                        label={`Bairro`}
                        info={checkpoint.address_district}
                    />
                    <LabelInfo
                        label={`Cidade/US`}
                        info={`${checkpoint.address_state}/${checkpoint.address_city}`}
                    />
                </Block>
                {(this.props.labelStatus && this.props.labelStatus !== "NOT_STATED") &&
                <TouchableOpacity
                    onPress={() => this.props.actionStatus()}
                    style={{
                        backgroundColor: 'blue',
                        marginLeft: -(theme.SIZES.BASE / 2 + 8),
                        marginRight: -(theme.SIZES.BASE / 2),
                        marginBottom: -(theme.SIZES.BASE / 2),
                        borderBottomRightRadius: 4,
                        borderBottomLeftRadius: 4,
                        paddingVertical: 5
                    }}
                >
                    <Text style={{width: '100%', textAlign: 'center'}}>{this.props.labelStatus}</Text>
                </TouchableOpacity>
                }
            </Block>
        );
    }
}

CheckpointCard.propTypes = {
    horizontal: PropTypes.bool,
    full: PropTypes.bool,
    ctaColor: PropTypes.string,
    imageStyle: PropTypes.any,
    ctaRight: PropTypes.bool
};

const styles = StyleSheet.create({
    card: {
        backgroundColor: theme.COLORS.WHITE,
        marginVertical: theme.SIZES.BASE,
        borderWidth: 0,
        minHeight: 114,
        marginBottom: 4,
    },
    cardTitle: {
        // flex: 1,
        // flexWrap: "wrap",
        paddingBottom: 6
    },
    cardDescription: {
        padding: theme.SIZES.BASE / 2
    },
    imageContainer: {
        borderRadius: 3,
        elevation: 1,
        overflow: "hidden"
    },
    image: {
        // borderRadius: 3,
    },
    horizontalImage: {
        height: 122,
        width: "auto"
    },
    horizontalStyles: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    verticalStyles: {
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0
    },
    fullImage: {
        height: 215
    },
    shadow: {
        shadowColor: "#8898AA",
        shadowOffset: {width: 0, height: 1},
        shadowRadius: 6,
        shadowOpacity: 0.1,
        elevation: 2
    }
});

export default withNavigation(CheckpointCard);
