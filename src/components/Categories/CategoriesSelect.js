import React from "react";
import PropTypes from "prop-types";
import Spinner from "../UI/Spinner";
import {getCategories} from "../../../actions/category";
import {connect} from "react-redux";
import InputSelect from "../UI/InputSelect";
import AlertNotFound from "../UI/AlertNotFound";

class CategoriesSelect extends React.Component {

    componentDidMount() {
        this.props.getCategories();
    }

    render() {

        const {categoriesLoading, categories, onValueChange, value} = this.props;

        if (categoriesLoading) {
            return (<Spinner/>)
        } else {

            if (categories && categories.length > 0) {
                const items = categories.map((category) => {
                    return {
                        label: category.name,
                        value: category.id,
                        key: category.id,
                    }
                });

                return <InputSelect
                    value={value}
                    onValueChange={onValueChange}
                    options={items}
                    placeholder={"Selecione uma categoria"}
                />
            } else {
                return <AlertNotFound label='Nenhuma categorias encontrada!'/>
            }
        }
    }
}

CategoriesSelect.propTypes = {
    onValueChange: PropTypes.func.isRequired,
    value: PropTypes.any,
};


//Map the redux state to your props.
const mapStateToProps = state => ({
    user: state.User.user,
    token: state.User.token,
    categoriesLoading: state.Category.isLoading,
    categories: state.Category.categories,
});

//Map your action creators to your props.
const mapDispatchToProps = {
    getCategories
};

//export your list as a default export
export default connect(mapStateToProps, mapDispatchToProps)(CategoriesSelect);
