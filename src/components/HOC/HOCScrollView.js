import React from 'react'
import {Dimensions, ScrollView, View, TouchableWithoutFeedback} from "react-native";
import {theme} from "galio-framework";
import PropTypes from "prop-types";


class HOCScrollView extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {width, height} = Dimensions.get("screen");

        const style = {
            ...this.props.style,
            paddingHorizontal: this.props.paddingHorizontal ? 2 : 0,
        };

        if (this.props.hidePaddingHorizontal) {
            style.width = width;
        } else {
            style.width = width - theme.SIZES.BASE * 2;
        }
        if (this.props.hidePaddingHorizontal) {
            style.paddingVertical = 0;
        } else {
            style.paddingVertical = theme.SIZES.BASE;
        }

        return (
            <TouchableWithoutFeedback onPress={() => {}}>
                <ScrollView showsVerticalScrollIndicator={true} contentContainerStyle={style}>
                    <View onStartShouldSetResponder={() => true}>
                        {this.props.children}
                    </View>
                </ScrollView>

            </TouchableWithoutFeedback>
        )
    }
}

HOCScrollView.propTypes = {
    hidePaddingHorizontal: PropTypes.bool,
    hidePaddingVertical: PropTypes.bool,
    style: PropTypes.object,
};
export default HOCScrollView;