import React from 'react'
import {Dimensions, ImageBackground} from "react-native";
import {Block, theme} from "galio-framework";
import PropTypes from "prop-types";
import HOCScrollView from "./HOCScrollView";
import ModalSpinner from "../UI/ModalSpinner";
import HOCDismissKeyboard from "./HOCDismissKeyboard";

import {KeyboardAvoidingView} from 'react-native';


class HOCBackground extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hideModalSpinner: false
        }
    }

    componentWillUnmount() {
        this.setState({
            hideModalSpinner: true
        })
    }

    render() {

        const {width, height} = Dimensions.get("screen");

        return (
            <ImageBackground style={{width, height, zIndex: 1}}>

                <HOCDismissKeyboard>
                    <Block flex center style={{width: width}}>
                        <KeyboardAvoidingView behavior="padding" enabled>
                            <HOCScrollView hidePaddingHorizontal={this.props.hidePaddingHorizontal}
                                           hidePaddingVertical={this.props.hidePaddingVertical}>
                                {this.state.hideModalSpinner ? null :
                                    <ModalSpinner isOpen={this.props.isLoadingModal}/>}
                                {this.props.children}
                            </HOCScrollView>
                        </KeyboardAvoidingView>
                    </Block>
                </HOCDismissKeyboard>

            </ImageBackground>
        )
    }
}

HOCBackground.propTypes = {
    hidePaddingHorizontal: PropTypes.bool,
    hidePaddingVertical: PropTypes.bool,
    style: PropTypes.object,
    isLoadingModal: PropTypes.bool
};
export default HOCBackground;