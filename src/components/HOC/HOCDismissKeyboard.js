import React from 'react'
import {Keyboard, TouchableWithoutFeedback} from "react-native";

class HOCDismissKeyboard extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (

            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                {this.props.children}
            </TouchableWithoutFeedback>
        )
    }
}

export default HOCDismissKeyboard;