import React from "react";
import {withNavigation} from "react-navigation";
import PropTypes from "prop-types";
import moment from 'moment'
import {
    StyleSheet, TouchableOpacity
} from "react-native";
import {Block, Text, theme} from "galio-framework";

import {argonTheme, Images} from "../../constants";
import Divider from "../UI/Divider";
import {distanceFormat} from "../../services/Helpers";
import Heading from "../UI/Heading";
import LabelInfo from "../UI/LabelInfo";

class OrderSingle extends React.Component {
    render() {
        const {navigation, order, horizontal, full, style, imageStyle, actionStatus, labelStatus} = this.props;

        return (
            <Block card style={[styles.shadow, styles.card, {
                padding: theme.SIZES.BASE / 2,
                borderLeftColor: 'blue',
                borderLeftWidth: 8
            }]}>
                <Block flex row space={'between'}>
                    <Block flex={1} center style={{backgroundColor: 'blue', borderRadius: 4, padding: 3}}>
                        <Text style={{fontSize: 8}}>{order.status}</Text>
                    </Block>
                    <Block flex={5}>
                        <Heading
                            inverted
                            size={'xlarge'}
                            label={`Corrida #${order && order.id || 0}`}
                            style={{textAlign: 'center'}}
                        />
                    </Block>
                </Block>
                <Block flex left style={{
                    width: '100%',
                    padding: theme.SIZES.BASE / 2
                }}>
                    <Text style={styles.cardLabel}>Descrição</Text>
                    <Text style={{fontWeight: 'bold'}}>{order && order.description || null}</Text>
                </Block>
                <Divider vmargin={"tiny"}/>
                <Block flex left style={{
                    width: '100%',
                    padding: theme.SIZES.BASE / 2
                }}>
                    <Block flex row space={'between'} style={{marginVertical: 5}}>
                        <LabelInfo
                            label={`Distancia`}
                            info={distanceFormat(order.total_distance)}
                        />
                        <LabelInfo
                            label={`Distancia`}
                            info={distanceFormat(order.total_distance)}
                        />
                    </Block>
                    <Block flex row space={'between'} style={{marginVertical: 5}}>
                        <LabelInfo
                            label={`Distancia`}
                            info={distanceFormat(order.total_distance)}
                        />
                        <LabelInfo
                            label={`Distancia`}
                            size={'xlarge'}
                            info={distanceFormat(order.total_distance)}
                        />
                    </Block>

                </Block>
                {labelStatus &&
                <TouchableOpacity
                    onPress={() => actionStatus()}
                    style={{
                        backgroundColor: 'blue',
                        marginLeft: -(theme.SIZES.BASE / 2 + 8),
                        marginRight: -(theme.SIZES.BASE / 2),
                        marginBottom: -(theme.SIZES.BASE / 2),
                        borderBottomRightRadius: 4,
                        borderBottomLeftRadius: 4,
                        paddingVertical: 5
                    }}
                >
                    <Text style={{width: '100%', textAlign: 'center'}}>{labelStatus}</Text>
                </TouchableOpacity>
                }
            </Block>
        );
    }
}

OrderSingle.propTypes = {
    order: PropTypes.object,
    horizontal: PropTypes.bool,
    full: PropTypes.bool,
    ctaColor: PropTypes.string,
    imageStyle: PropTypes.any,
    ctaRight: PropTypes.bool
};

const styles = StyleSheet.create({
    card: {
        backgroundColor: theme.COLORS.WHITE,
        marginVertical: theme.SIZES.BASE,
        borderWidth: 0,
        minHeight: 114,
        marginBottom: 4,
    },
    cardTitle: {
        // flex: 1,
        // flexWrap: "wrap",
        paddingBottom: 6
    },
    cardDescription: {
        paddingHorizontal: theme.SIZES.BASE / 2,
        paddingTop: theme.SIZES.BASE / 2,
        borderRadius: 4,
    },
    cardLabel: {
        fontSize: 10,
        marginBottom: 5,
        color: argonTheme.COLORS.MUTED,
        fontWeight: 'bold'
    },
    imageContainer: {
        borderRadius: 3,
        elevation: 1,
        overflow: "hidden"
    },
    image: {
        // borderRadius: 3,
    },
    horizontalImage: {
        height: 122,
        width: "auto"
    },
    horizontalStyles: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    verticalStyles: {
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0
    },
    fullImage: {
        height: 215
    },
    shadow: {
        shadowColor: "#8898AA",
        shadowOffset: {width: 0, height: 1},
        shadowRadius: 6,
        shadowOpacity: 0.1,
        elevation: 2
    }
});

export default withNavigation(OrderSingle);
