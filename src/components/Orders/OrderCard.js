import React from "react";
import {withNavigation} from "react-navigation";
import PropTypes from "prop-types";
import moment from 'moment'
import {
    StyleSheet,
    Image,
    TouchableWithoutFeedback, TouchableOpacity
} from "react-native";
import {Block, Text, theme} from "galio-framework";

import {argonTheme, Images} from "../../constants";
import {Button} from "../index";
import Divider from "../UI/Divider";
import {distanceFormat} from "../../services/Helpers";

class OrderCard extends React.Component {
    render() {
        const {navigation, order, horizontal, full, style, ctaColor, imageStyle, ctaRight} = this.props;

        const imageStyles = [
            full ? styles.fullImage : styles.horizontalImage,
            imageStyle
        ];
        const cardContainer = [styles.card, styles.shadow, style];

        let imgContainer = [
            // styles.imageContainer,
            horizontal ? styles.horizontalStyles : styles.verticalStyles,
        ];
        let image = Images.IconLaranja;

        // if (order.image_1) {
        //     imgContainer = [
        //         styles.imageContainer,
        //         horizontal ? styles.horizontalStyles : styles.verticalStyles,
        //         styles.shadow
        //     ];
        //     image = {uri: order.image_1};
        // }

        return (
            <Block row={horizontal} card flex style={cardContainer}>

                <Block flex space="between" style={styles.cardDescription}>
                    <Block flex style={{
                        backgroundColor: 'white',
                        borderRadius: 4,
                        paddingHorizontal: 10,
                        paddingTop: 5
                    }}>
                        <Block flex row>
                            <Block flex>
                                <Text style={styles.cardLabel}>Data/Hora</Text>
                                <Text style={{
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    marginBottom: 5
                                }}>{moment(order.ordered_at).format("DD/MM/YYYY HH:mm")}</Text>
                                <Text style={styles.cardLabel}>Duração</Text>
                                <Text style={{fontSize: 10, fontWeight: 'bold'}}>{order.total_duration} min.</Text>
                            </Block>
                            <Block flex>
                                <Text style={styles.cardLabel}>Distancia</Text>
                                <Text style={{
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    marginBottom: 5
                                }}>{distanceFormat(order.total_distance)}</Text>
                                <Text style={styles.cardLabel}>Forma de Pagamento</Text>
                                <Text style={{
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    marginBottom: 5
                                }}>{moment(order.ordered_at).format("DD/MM/YYYY HH:mm")}</Text>
                            </Block>
                        </Block>
                        <Divider vmargin={"tiny"}/>
                        <Block flex style={[styles.shadow, {
                            marginHorizontal: -20,
                            borderLeftWidth: 8,
                            borderLeftColor: argonTheme.COLORS.ACTIVE,
                            borderRadius: 4,
                            padding: 13,
                            borderRightColor: argonTheme.COLORS.ACTIVE,
                            borderRightWidth: 8
                        }]}>
                            <Text style={styles.cardLabel}>Descrição</Text>
                            <Text style={{fontWeight: 'bold'}}>{order.description}</Text>
                            <Block right>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('OrderView', {order_id: order.id})}
                                >
                                    <Text
                                        style={{fontFamily: 'open-sans-bold'}}
                                        size={10}

                                        color={argonTheme.COLORS.ACTIVE}
                                        bold
                                    >
                                        Ver mais
                                    </Text>
                                </TouchableOpacity>
                            </Block>
                        </Block>
                    </Block>
                </Block>
            </Block>
        );
    }
}

OrderCard.propTypes = {
    order: PropTypes.object,
    horizontal: PropTypes.bool,
    full: PropTypes.bool,
    ctaColor: PropTypes.string,
    imageStyle: PropTypes.any,
    ctaRight: PropTypes.bool
};

const styles = StyleSheet.create({
    card: {
        backgroundColor: theme.COLORS.WHITE,
        marginVertical: theme.SIZES.BASE,
        borderWidth: 0,
        minHeight: 114,
        marginBottom: 4,
    },
    cardTitle: {
        // flex: 1,
        // flexWrap: "wrap",
        paddingBottom: 6
    },
    cardDescription: {
        paddingHorizontal: theme.SIZES.BASE / 2,
        paddingTop: theme.SIZES.BASE / 2,
        borderRadius: 4,
    },
    cardLabel: {
        fontSize: 10,
        marginBottom: 5,
        color: argonTheme.COLORS.MUTED,
        fontWeight: 'bold'
    },
    imageContainer: {
        borderRadius: 3,
        elevation: 1,
        overflow: "hidden"
    },
    image: {
        // borderRadius: 3,
    },
    horizontalImage: {
        height: 122,
        width: "auto"
    },
    horizontalStyles: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    verticalStyles: {
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0
    },
    fullImage: {
        height: 215
    },
    shadow: {
        shadowColor: "#8898AA",
        shadowOffset: {width: 0, height: 1},
        shadowRadius: 6,
        shadowOpacity: 0.1,
        elevation: 2
    }
});

export default withNavigation(OrderCard);
