import {theme} from "galio-framework";

export default {
  MODAL_WIDTH : 300,
  MODAL_INPUT_WIDTH: 300 - theme.SIZES.BASE,
  COLORS: {
    MODAL_BACKGROUND: '#ebebeb',
    DEFAULT: '#172B4D',
    // PRIMARY: '#5E72E4',
    PRIMARY: '#77e742',
    SECONDARY: '#F7FAFC',
    LABEL: '#FE2472',
    INFO: '#11CDEF',
    ERROR: '#F5365C',
    SUCCESS: '#2DCE89',
    WARNING: '#FB6340',
    TEXT: '#32325D',
    MUTED: '#8898AA',
    INPUT: '#DCDCDC',
    INPUT_SUCCESS: '#7BDEB2',
    INPUT_ERROR: '#FCB3A4',
    // ACTIVE: '#5E72E4', //same as primary
    ACTIVE: '#77e742', //same as primary
    BUTTON_COLOR: '#9C26B0', //wtf
    PLACEHOLDER: '#9FA5AA',
    SWITCH_ON: '#5E72E4',
    SWITCH_OFF: '#D4D9DD',
    GRADIENT_START: '#6B24AA',
    GRADIENT_END: '#AC2688',
    PRICE_COLOR: '#EAD5FB',
    BORDER_COLOR: '#E7E7E7',
    BLOCK: '#E7E7E7',
    ICON: '#172B4D',
    HEADER: '#227f1e',
    BORDER: '#CAD1D7',
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    TWITTER: '#1DA1F2',
    FACEBOOK: '#3B5999',
    DRIBBBLE: '#EA4C89',
  }
};