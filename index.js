import Avatar from './src/components/UI/Avatar'
import ButtonSetting from "./src/components/UI/ButtonSetting";
import Carrossel from "./src/components/Carrossel";
import GenderSelect from "./src/components/User/GenderSelect";
import Heading from "./src/components/UI/Heading";
import InputDatePicker from "./src/components/UI/InputDatePicker";
import InputDefault from "./src/components/UI/InputDefault";
import InputSearch from "./src/components/UI/InputSearch";
import HOCBackground from "./src/components/HOC/HOCBackground";
import StateSelect from "./src/components/User/StateSelect";
import FormButton from "./src/components/UI/FormButton";
import FormButtonLink from "./src/components/UI/FormButtonLink";
import InputMultiline from "./src/components/UI/InputMultiline"
import NumberCurrency from "./src/components/UI/NumberCurrency";
import ModalConfirmation from "./src/components/UI/ModalConfirmation";
import ModalRating from "./src/components/Rating/ModelRating"
import RatingShow from "./src/components/Rating/RatingShow";
import Checkbox from "./src/components/UI/Checkbox"
import ModalSingle from "./src/components/UI/ModalSingle";

export {
    Avatar,
    ButtonSetting,
    Carrossel,
    GenderSelect,
    Heading,
    InputDatePicker,
    InputDefault,
    InputSearch,
    HOCBackground,
    NumberCurrency,
    ModalConfirmation,
    ModalRating,
    StateSelect,
    FormButton,
    FormButtonLink,
    InputMultiline,
    RatingShow,
    Checkbox,
    ModalSingle,
}
